package axxonsoftinterview

import (
	"context"
	"github.com/google/uuid"
)

type AccessList interface {
	HasAccess(ctx context.Context, client, resource string) (bool, error)
	Add(ctx context.Context, client string, resources ...string) error
	Remove(ctx context.Context, client string, resources ...string) error
}

type RequestRepository interface {
	Get(ctx context.Context, uid uuid.UUID) (*Request, error)
	Store(ctx context.Context, request *Request) error
	Delete(ctx context.Context, uid uuid.UUID) error
}
