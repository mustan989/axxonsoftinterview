package axxonsoftinterview

import (
	"context"
	"sync"
)

func NewInmemoryAccessList() AccessList {
	return &accessList{clientResources: make(map[string][]string)}
}

type accessList struct {
	mu              sync.RWMutex
	clientResources map[string][]string
}

func (al *accessList) HasAccess(ctx context.Context, client, resource string) (bool, error) {
	list, ok := al.clientResources[client]
	if !ok {
		// BUG: stack overflow if '*' value is not set (on init)
		// TODO: possible code optimisations?
		if client == "*" {
			return false, nil
		}
		return al.HasAccess(ctx, "*", resource)
	}

	for _, h := range list {
		if h == "*" || h == resource {
			return true, nil
		}
	}

	return false, nil
}

func (al *accessList) Add(_ context.Context, client string, resources ...string) error {
	al.mu.Lock()
	defer al.mu.Unlock()

	list, ok := al.clientResources[client]
	if !ok {
		list = make([]string, 0)
	}

	list = append(list, resources...)

	al.clientResources[client] = list

	return nil
}

func (al *accessList) Remove(_ context.Context, client string, resources ...string) error {
	al.mu.Lock()
	defer al.mu.Unlock()

	if len(resources) == 0 || resources[0] == "*" {
		delete(al.clientResources, client)
		return nil
	}

	list, ok := al.clientResources[client]
	if !ok {
		return nil
	}

	for _, resource := range resources {
		for i, r := range list {
			if r == resource {
				if i+1 == len(list) {
					list = append(list[:i])
				} else {
					list = append(list[:i], list[i+1:]...)
				}
			}
		}
	}

	al.clientResources[client] = list

	return nil
}
