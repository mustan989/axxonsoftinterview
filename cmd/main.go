package main

import (
	"context"
	"errors"
	"fmt"
	"github.com/google/uuid"
	"github.com/labstack/echo/v4"
	"gitlab.com/mustan989/axxonsoftinterview"
	"log"
	"net/http"
)

func main() {
	infof("initializing app...")
	// read config, get access list, etc...

	list := axxonsoftinterview.NewInmemoryAccessList()
	// TODO: mv to config
	{
		if err := list.Add(context.TODO(), "*", "*"); err != nil {
			errorf("error adding record to access list: %s", err)
		}
	}

	proxy := axxonsoftinterview.NewProxy(axxonsoftinterview.WithAccessList(list))

	server := echo.New()

	server.POST("/requests/do", func(c echo.Context) error {
		var request axxonsoftinterview.Request
		if err := c.Bind(&request); err != nil {
			return c.JSON(http.StatusBadRequest, res{err})
		}

		if request.Payload != nil {
			request.Payload.Length = int64(len(request.Payload.Content))
		}

		if err := proxy.DoRequest(c.Request().Context(), c.Request().RemoteAddr, &request); err != nil {
			return c.JSON(handleError(err))
		}

		c.Response().Header().Set(echo.HeaderLocation, fmt.Sprintf("/requests/%s", request.ID))

		return c.JSON(http.StatusOK, request.Response)
	})

	server.GET("/requests/:id", func(c echo.Context) error {
		id, err := uuid.Parse(c.Param("id"))
		if err != nil {
			return c.JSON(http.StatusBadRequest, res{"invalid id value"})
		}

		request, err := proxy.GetRequest(c.Request().Context(), id)
		if err != nil {
			return c.JSON(handleError(err))
		}

		return c.JSON(http.StatusOK, request)
	})

	infof("app starting...")
	// TODO: mv host, port to config
	errorf("error on http server running: %s", server.Start(":9090"))
}

type res struct {
	Error any `json:"error"`
}

func handleError(err error) (code int, body any) {
	var cErr *axxonsoftinterview.CustomErr
	if errors.As(err, &cErr) {
		return cErr.Code, cErr
	}

	var httpErr *echo.HTTPError
	if errors.As(err, &httpErr) {
		return httpErr.Code, res{httpErr}
	}
	return http.StatusInternalServerError, res{err}
}

// TODO: mv to lib, pkg
func infof(format string, a ...any) {
	log.Println("INFO", fmt.Sprintf(format, a...))
}

func warnf(format string, a ...any) {
	log.Println("WARN", fmt.Sprintf(format, a...))
}

func errorf(format string, a ...any) {
	log.Println("ERROR", fmt.Sprintf(format, a...))
}
