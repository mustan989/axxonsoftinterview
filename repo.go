package axxonsoftinterview

import (
	"context"
	"github.com/google/uuid"
	"sync"
)

func NewInmemoryRepository() RequestRepository {
	return &requestRepo{requests: make(map[uuid.UUID]*Request)}
}

type requestRepo struct {
	mu       sync.RWMutex
	requests map[uuid.UUID]*Request
}

func (r *requestRepo) Get(_ context.Context, id uuid.UUID) (*Request, error) {
	request, ok := r.requests[id]
	if !ok {
		return nil, notFound("request with given id not found")
	}

	return request, nil
}

func (r *requestRepo) Store(_ context.Context, request *Request) error {
	r.mu.Lock()
	defer r.mu.Unlock()

	r.requests[request.ID] = request

	return nil
}

func (r *requestRepo) Delete(_ context.Context, id uuid.UUID) error {
	r.mu.RLock()
	defer r.mu.RUnlock()

	delete(r.requests, id)

	return nil
}
