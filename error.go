package axxonsoftinterview

import (
	"fmt"
	"net/http"
)

func notFound(msg string) error {
	return &CustomErr{http.StatusNotFound, msg, ""}
}

type CustomErr struct {
	// Code HTTP code for error. TODO: mv to custom iota type with own converters
	Code int    `json:"-"`
	Info string `json:"info"`
	Err  string `json:"error,omitempty"`
}

func (e *CustomErr) Error() string {
	if e.Err == "" {
		return e.Info
	}
	return fmt.Sprintf("%s: %s", e.Info, e.Err)
}
