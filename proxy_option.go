package axxonsoftinterview

import "net/http"

type ProxyOption func(p *Proxy)

func WithRequestRepository(repository RequestRepository) ProxyOption {
	return func(p *Proxy) {
		p.rr = repository
	}
}

func WithAccessList(access AccessList) ProxyOption {
	return func(p *Proxy) {
		p.access = access
	}
}

func WithHTTPClient(client *http.Client) ProxyOption {
	return func(p *Proxy) {
		p.client = client
	}
}
