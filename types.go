package axxonsoftinterview

import (
	"github.com/google/uuid"
)

type Request struct {
	ID      uuid.UUID         `json:"id,omitempty"`
	Method  string            `json:"method"`
	Url     string            `json:"url"`
	Headers map[string]string `json:"headers"`
	Payload *Payload          `json:"payload"`

	Response *Response `json:"response,omitempty"`
}

type Response struct {
	ID     uuid.UUID `json:"id"`
	Status struct {
		Code    int    `json:"code"`
		Meaning string `json:"meaning"`
	} `json:"status"`
	Headers map[string]string `json:"headers"`
	Payload *Payload          `json:"payload"`
}

type Payload struct {
	Length  int64  `json:"length"`
	Content []byte `json:"content"`
}
