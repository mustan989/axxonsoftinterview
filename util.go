package axxonsoftinterview

import (
	"fmt"
	"io"
	"net/http"
)

func HeadersToMap(header http.Header) map[string]string {
	m := make(map[string]string)
	for key := range header {
		m[key] = header.Get(key)
	}
	return m
}

func ParseResponse(res *http.Response) (*Response, error) {
	response := Response{
		Status: struct {
			Code    int    `json:"code"`
			Meaning string `json:"meaning"`
		}{
			Code:    res.StatusCode,
			Meaning: res.Status,
		},
		Headers: HeadersToMap(res.Header),
	}

	if res.ContentLength != 0 {
		defer res.Body.Close()

		b, err := io.ReadAll(res.Body)
		if err != nil {
			return nil, fmt.Errorf("error reading response body: %s", err)
		}

		response.Payload = &Payload{res.ContentLength, b}
	}

	return &response, nil
}
