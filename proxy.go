package axxonsoftinterview

import (
	"bytes"
	"context"
	"github.com/google/uuid"
	"io"
	"net/http"
	"net/url"
)

func NewProxy(opts ...ProxyOption) *Proxy {
	p := Proxy{
		access: NewInmemoryAccessList(),
		client: http.DefaultClient,
		rr:     NewInmemoryRepository(),
	}
	for _, opt := range opts {
		opt(&p)
	}
	return &p
}

type Proxy struct {
	access AccessList
	client *http.Client
	rr     RequestRepository
}

func (p *Proxy) DoRequest(ctx context.Context, clientAddr string, request *Request) error {
	// TODO: handle
	defer p.rr.Store(ctx, request)

	id, err := uuid.NewUUID()
	if err != nil {
		return &CustomErr{http.StatusInternalServerError, "error generating unique id", err.Error()}
	}
	request.ID = id

	// check if client has access to resource
	reqUrl, err := url.Parse(request.Url)
	if err != nil {
		return &CustomErr{http.StatusBadRequest, "invalid url", err.Error()}
	}

	if ok, _ := p.access.HasAccess(ctx, clientAddr, reqUrl.Host); !ok {
		return &CustomErr{http.StatusForbidden, "client has no access to given resource", ""}
	}

	// TODO: possible mv to func?
	{
		var payload io.Reader
		if request.Payload != nil {
			payload = bytes.NewBuffer(request.Payload.Content)
		}

		req, err := http.NewRequestWithContext(ctx, request.Method, request.Url, payload)
		if err != nil {
			return &CustomErr{http.StatusInternalServerError, "error creating request", err.Error()}
		}

		for k, v := range request.Headers {
			req.Header.Set(k, v)
		}

		res, err := p.client.Do(req)
		if err != nil {
			return &CustomErr{http.StatusInternalServerError, "error executing request", err.Error()}
		}

		request.Response, err = ParseResponse(res)
		if err != nil {
			return &CustomErr{http.StatusInternalServerError, "error parsing response", err.Error()}
		}

		request.Response.ID = id
	}

	return nil
}

func (p *Proxy) GetRequest(ctx context.Context, id uuid.UUID) (*Request, error) {
	return p.rr.Get(ctx, id)
}
