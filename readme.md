# Axxonsoft Interview


## Task

Write HTTP server for proxying HTTP-requests to 3rd-party services.
The server is waiting HTTP-request from client (curl, for example). In request's body there should be message in JSON
format. For example:

```json
{
  "method": "GET",
  "url": "http://google.com",
  "headers": {
    "Authentication": "Basic bG9naW46cGFzc3dvcmQ=",
    ....
  }
}
```

Server forms valid HTTP-request to 3rd-party service with data from client's message and responses to client with JSON
object:

```json
{
  "id": <generated unique id>,
  "status": <HTTP status of 3rd-party service response>,
  "headers": {
    <headers array from 3rd-party service response>
  },
  "length": <content length of 3rd-party service response>
}
```

Server should have map to store requests from client and responses from 3rd-party service


## Usage

1. Run `go run cmd/main.go`
2. Call `POST /requests/do --data '$payload'`
3. Get response
4. Get both request & response by id from response body or `Location` header `GET /requests/$id`

Request & response payloads described in [types.go](types.go)


## TODO

* customizations (config, etc.)
* request validation
* tests
* read access list from file + can be changed in runtime